﻿using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    using System;
    using System.Linq;
    using Microsoft.EntityFrameworkCore.Internal;

    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly DataContext _dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            //todo rem
            Console.WriteLine(dataContext.Database.ProviderName);

            _dataContext = dataContext;
        }
        
        public void InitializeDb()
        {
            if (this._dataContext.Preferences.Any()) return;

            this._dataContext.AddRange(FakeDataFactory.Employees);
            this._dataContext.SaveChanges();

            this._dataContext.AddRange(FakeDataFactory.Preferences);
            this._dataContext.SaveChanges();

            this._dataContext.AddRange(FakeDataFactory.Customers);
            this._dataContext.SaveChanges();
        }
    }
}